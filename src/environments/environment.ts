export const environment = {
  production: false,
  encrypt_key: "S0MKVeOwgD",
  user_endpoint: "http://localhost:8080/matonoha.web/api/user",
  data_endpoint: "http://localhost:8080/matonoha.web/api/data",
  auth_endpoint: "http://localhost:8080/matonoha.web/api/auth"
};


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterXmlComponent } from './register-xml.component';

describe('RegisterXmlComponent', () => {
  let component: RegisterXmlComponent;
  let fixture: ComponentFixture<RegisterXmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterXmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterXmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginXmlComponent } from './login-xml.component';

describe('LoginXmlComponent', () => {
  let component: LoginXmlComponent;
  let fixture: ComponentFixture<LoginXmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginXmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginXmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

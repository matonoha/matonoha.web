import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { HttpParams } from '@angular/common/http'

@Injectable()
export class DataService {
  image_data: any;
  user_data: any;

  constructor(protected http: HttpClient,
    protected parameters: HttpParams) {}
  
  requestUserData() {
    let response = this.http.get(environment.user_endpoint + "?token=all").subscribe((x) => this.user_data = x);
    this.user_data = this.formatJSON(this.user_data);
  }

  private formatJSON(dataJSON) {
    return JSON.stringify(dataJSON);
  }

  postUserData(name: String, email: String, hashed_pwd: String, token: String) {
    console.log('Hashed password: ' + hashed_pwd);
    let data = {
      "name": name,
      "email": email,
      "password": hashed_pwd,
      "token": token,
      "debug": true
    };

    console.log("Post user data: " + data);
    
    this.http.post<any>(environment.user_endpoint, data).subscribe();
  }

  updateUserData(name: string, email: string, token: string) {
    let updated = {
      "name": name,
      "email": email,
      "token": token 
    };

    this.http.put<any>(environment.user_endpoint, updated).subscribe();
  }

  deleteUser(token: String) {
    let path = environment.user_endpoint + "?token=" + token
    return this.http.delete<any>(path).toPromise()
  }
}
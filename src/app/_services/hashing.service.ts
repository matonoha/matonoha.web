import { Injectable } from '@angular/core';
import * as crypto from 'crypto-js';

@Injectable()
export class HashingService {
    constructor() {}

    encrypt(key: any, privateKey: any) {
        const pkey = crypto.enc.Utf8.parse(privateKey);
        const encrypted = crypto.AES.encrypt(key, pkey, {
            iv: crypto.enc.Hex.parse('00000000000000000000000000000000'),
            mode: crypto.mode.ECB,
            padding: crypto.pad.Pkcs7
        }).toString();

        return encrypted;
    }

    getToken() {
        return crypto.lib.WordArray.random(32);
    }
}
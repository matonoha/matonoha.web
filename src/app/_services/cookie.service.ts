import { Injectable } from '@angular/core';

@Injectable()
export class CookieService{
  constructor() {}

   private getCookie(name: String) {
     let cookie_array: Array<String> = document.cookie.split(';');
     let cookie_name = `${name}=`;

    cookie_array.forEach(element => {
        element = element.replace(/^\s+/g, '');

        if(element.indexOf(cookie_name) == 0) {
            return element.substring(cookie_name.length, element.length);
        }
    });
      return '';
  }

  setCookie(name: String, value: String, expire_date: number, path: String = '') {
    let date: Date = new Date();
    expire_date = expire_date * 24 * 60 * 60 * 1000;
    date.setTime(date.getTime() + expire_date);
    
    let expires: string = `expires=${date.toUTCString()}`;
    let cpath:string = path ? `; path=${path}` : '';
    document.cookie = `${name}=${value}; ${expires}${cpath}`;
}

private deleteCookie(name) {
    this.setCookie(name, '', -1);
  }


}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../_models/user';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AccountService {
    data: any;
    private response: any;
    private acc_exists: any;
    private logged_in: any;
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    constructor(private api: HttpClient,
        private router: Router) {}

    isTaken(column: String, value: String){
        return this.api.get(
            environment.auth_endpoint
            + "=" + column 
            + "&value=" + value)
            .toPromise();
    }

    login(name: String, email: String, password: String) {
        return this.api.post(environment.auth_endpoint, {"name": name, "email": email, "password": password}).toPromise();
    }

    getLogin() {
        return this.logged_in;
    }

    setLogin(value: boolean) {
        this.logged_in = value
        this.acc_exists = value
    }

    get userValue(): User {
        return this.userSubject.value;
    }

    loginUser(username: string, password: string) {
        return this.api.post<User>(
            `${environment.data_endpoint}/users/authenticate`,
            {username, password}).pipe(map(user => {
                localStorage.setItem('user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/account/login']);
    }

    delete(token: string) {
        return this.api.delete(`${environment.data_endpoint}/users/${token}`)
            .pipe(map(x => {
                if (token == this.userValue.key_token) {
                    this.logout();
                }
            }));
    }

    update(token: string, params) {
        return this.api.put(`${environment.data_endpoint}/users/${token}`, params)
            .pipe(map(x => {
                if (token == this.userValue.key_token) {
                    const user = {... this.userValue, ... params};
                    localStorage.setItem('user', JSON.stringify(user));
                }
            }));
    }

    register(name: string, password: string, email: string, token: string) {
        let user = {
            "name": name,
            "password": password,
            "email": email,
            "token": token
        }
        console.log('Registering with password: ' + password)
        return this.api.post(environment.user_endpoint, user);
    }

}
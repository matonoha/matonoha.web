import { Component, OnInit, Injectable } from '@angular/core';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  constructor(private account: AccountService) { }

  ngOnInit(): void {
    
  }

  isLoggedIn() {
    return this.account.getLogin();
  }

  logout() {
    console.log("Logging out");
    this.account.setLogin(false);
  }

}

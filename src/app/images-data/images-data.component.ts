import { Component, OnInit } from '@angular/core';
import { DataService } from "../_services/data.service";
import { AccountService } from '../_services/account.service';
import { HashingService } from '../_services/hashing.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-images-data',
  templateUrl: './images-data.component.html',
  styleUrls: ['./images-data.component.css']
})
export class ImagesDataComponent implements OnInit {
  enableEdit = false;
  enableEditIndex = null;
  createNew = false;
  createButton = true;
  name: any;
  email: any;
  password: any;
  token: any;
  updateName: string;
  updateEmail: string;

  get jsonData() {
    return this.dataService.image_data; 
  }

  get userData() {
    return this.dataService.user_data;
  }

  constructor(protected dataService: DataService,
    protected account: AccountService,
    protected hash: HashingService) { }

  ngOnInit() {
    //this.dataService.requestPictureData();
    this.dataService.requestUserData();
  }


  getHeaders() {
    let headers: string[] = [];
    if(this.jsonData) {
      this.jsonData.forEach((value) => {
        Object.keys(value).forEach((key) => {
          if(!headers.find((header) => header == key)){
            headers.push(key)
          }
        })
      })
    }
    return headers;
  }

  getUserData() {
    let headers: string[] = [];
    if(this.userData) {
      this.userData.forEach((value) => {
        Object.keys(value).forEach((key) => {
          if(!headers.find((header) => header == key) && key != "data"){
            headers.push(key);
          }
        })
      })
    }
    return headers;
  }

  get isAdmin() {
    return true;
  }

  deleteUser(token: String) {
    console.log('User to delete: ' + token);
    this.dataService.deleteUser(token).then(x => location.reload());
  }

  enableEditMethod(e, i) {

    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
  }

  disableEditMethod(e, i) {
    this.enableEdit = false;
    this.enableEditIndex = i;
    console.log(i, e);
  }

  update(e, i) {
    // REAL UPDATE
    console.log('Updating name: ' + i['name']);
    console.log('Updating email: ' + i['email']);
    console.log('Updating for token: ' + i['token']); 

    this.dataService.updateUserData(i['name'], i['email'], i['token']);
  }

  show() {
      console.log("name: ", this.name);
      console.log("email: ", this.email);
      console.log("password: ", this.password);
      console.log("token: ", this.token)
      this.dataService.postUserData(this.name, this.email, this.password, this.token);
      this.createNew = false;

  }

  create() {
    this.createNew = true;
  }

  cancelCreate() {
    this.createNew = false;
  }

  setName(name: string) {
    this.name = name;
  }

  setEmail(email: string) {
    this.email = email;
  }

  setPassword(password: string) {
    this.password = this.hash.encrypt(password, environment.encrypt_key);
  }

  setToken(token: string) {
    if(token == '' || token == null || token == "undefined") {
      this.token = String(this.hash.getToken());
    } else {
      this.token = token;
    }
    
  }
  
}

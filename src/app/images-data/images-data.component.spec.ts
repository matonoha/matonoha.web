import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagesDataComponent } from './images-data.component';

describe('ImagesDataComponent', () => {
  let component: ImagesDataComponent;
  let fixture: ComponentFixture<ImagesDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagesDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

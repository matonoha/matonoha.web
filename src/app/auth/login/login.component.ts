import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HashingService } from '../../_services/hashing.service';
import { environment } from 'src/environments/environment';
import { AccountService } from 'src/app/_services/account.service';
import { Router, ɵROUTER_PROVIDERS, Routes} from '@angular/router';
import { CookieService } from 'src/app/_services/cookie.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'images-data',
    pathMatch: 'full'
  }
];

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ɵROUTER_PROVIDERS]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  username: any
  email: any
  password: any
  userToken: any
  acc_exists: any


  constructor(private formBuilder: FormBuilder,
    private hash: HashingService,
    private account: AccountService,
    private cookies: CookieService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  onSubmit() {
    if (this.loginForm.invalid) return;
    this.username = this.loginForm.value['name'];
    this.email = this.loginForm.value['email'];
    this.password = this.hash.encrypt(this.loginForm.value['password'], environment.encrypt_key);
    
    this.account.login(this.username, this.email, this.password)
      .then((data) => {
          if(data['user_exists'] && data['usr_token']) {
              this.acc_exists = true
              this.account.setLogin(true);
              this.cookies.setCookie('mm_login', this.userToken, 1, '/');   
          }
      });
  }
}

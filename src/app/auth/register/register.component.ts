import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HashingService } from '../../_services/hashing.service'
import { DataService } from "../../_services/data.service";
import { environment } from 'src/environments/environment';
import { AccountService } from 'src/app/_services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false
  usr_name: string
  usr_email: string
  usr_pwd: string
  usr_token: string


  constructor(
    private formBuilder: FormBuilder,
    private hash: HashingService,
    private accountService: AccountService
    ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      password_again: ['', Validators.required]
    })
  }
  
  onSubmit() {
    if (this.registerForm.invalid || this.registerForm.value['password'] != this.registerForm.value['password_again']) return;
    this.usr_name = this.registerForm.value['name'];
    this.usr_email = this.registerForm.value['email'];
    this.usr_token = String(this.hash.getToken());
    this.usr_pwd = this.hash.encrypt(this.registerForm.value['password'], environment.encrypt_key)

    this.accountService.login(this.usr_name, this.usr_email, this.usr_pwd).then((data) => {
      if(data['user_exists'] != true) {
        //console.log('User does not exist')
        this.accountService.register(this.usr_name, this.usr_pwd, this.usr_email, this.usr_token).subscribe()
      }
    })
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ImagesDataComponent } from './images-data/images-data.component'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpParams } from '@angular/common/http'
import { DataService } from './_services/data.service';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './auth/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HashingService } from './_services/hashing.service';
import { AccountService } from './_services/account.service';
import { AccountRoutingComponent } from './account/account-routing.component';
import { AccountLayoutComponent } from './account/account-layout.component';
import { AccountComponent } from './account/account.component';
import { UserDataComponent } from './user-data/user-data.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { CookieService } from './_services/cookie.service';
import { UserListComponent } from './user-list/user-list.component';
import { AuthenticationService } from './_services/auth.service';
import { LayoutComponent } from './users/layout/layout.component';
import { ListComponent } from './users/list/list.component';
import { LaunchListComponent } from './view/launch-list/launch-list.component';
import { DetailListComponent } from './view/detail-list/detail-list.component';
import { GraphQLModule } from './graphql.module';
import { MatCardModule } from '@angular/material/card';
import { LoginXmlComponent } from './pages_xml/auth/login-xml/login-xml.component';
import { RegisterXmlComponent } from './pages_xml/auth/register-xml/register-xml.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesDataComponent,
    HomeComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    AccountComponent,
    AccountLayoutComponent,
    UserDataComponent,
    UserListComponent,
    LayoutComponent,
    ListComponent,
    LaunchListComponent,
    DetailListComponent,
    LoginXmlComponent,
    RegisterXmlComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    GraphQLModule,
    MatCardModule
  ],
  providers: [ DataService, HashingService, AccountService,AuthenticationService, HttpParams, CookieService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

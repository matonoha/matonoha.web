import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagesDataComponent } from './images-data/images-data.component';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './auth/register/register.component';
import { UserDataComponent } from './user-data/user-data.component';
import { LaunchListComponent } from './view/launch-list/launch-list.component';
import { DetailListComponent } from './view/detail-list/detail-list.component';

const accountModule = () => import('./account/account.component');


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'images-data', component: ImagesDataComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'userinfo', component: UserDataComponent },
  {
    path: 'launch',
    component: LaunchListComponent
  },
  {
    path: 'launch/:id',
    component: DetailListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { AccountLayoutComponent } from '../account/account-layout.component';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { RegisterComponent } from 'src/app/auth/register/register.component';

const routes: Routes = [{
  path: '', component: AccountLayoutComponent,
  children: [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent }
  ]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingComponent { }

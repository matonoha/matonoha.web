import { Component, OnInit } from '@angular/core';
import { PastLaunchesListGQL } from '../../_services/spacexGraphql.service'
import { map } from 'rxjs/operators';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-launch-list',
  templateUrl: './launch-list.component.html',
  styleUrls: ['./launch-list.component.css']
})

export class LaunchListComponent implements OnInit {
  constructor(private pastLaunchService: PastLaunchesListGQL) { }

  pastLaunches$ = this.pastLaunchService.fetch({limit: 30})
    .pipe(map(
      res => res.data.launchesPast
    ))

  ngOnInit(): void {
  }

}

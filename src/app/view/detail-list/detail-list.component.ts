import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LaunchDetailsGQL } from 'src/app/_services/spacexGraphql.service';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-detail-list',
  templateUrl: './detail-list.component.html',
  styleUrls: ['./detail-list.component.css']
})
export class DetailListComponent {

  constructor(
    private readonly route: ActivatedRoute,
    private readonly launchDetailService: LaunchDetailsGQL
    ) { }

    launchDetails$ = this.route.paramMap.pipe(
      map((params) => params.get('id') as string),
      switchMap((id) => this.launchDetailService.fetch({id})),
      map((res) => res.data.launch)
    )

    comments = ['a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a']
}

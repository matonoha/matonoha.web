export class User {
    id: string;
    name: string;
    password: string;
    email: string;
    key_token: string;
}